package base;
import java.awt.Color;
import java.awt.Graphics;
import java.io.BufferedReader;
import java.io.InputStreamReader;

/**
 * @author Kevan Buckley, maintained by __student
 * @version 2.0, 2014
 */

public class Location extends SpacePlace {
  public int column;
  public int row;
  public DIRECTION direction;
  
  public enum DIRECTION {VERTICAL, HORIZONTAL};
  
  public Location(int r, int c) {
    this.r = r;
    this.c = c;
  }

  public Location(int r, int c, DIRECTION d) {    
    this(r,c);
    this.d=d;
  }
  
  public String toString() {
	  public int tempColumn;
	  public int tempRow;
	    if(direction==null){
	      return "(" + (tempColumn) + "," + (temporaryRow) + ")";
	    } else {
	    	return "(" + (tempColumn) + "," + (temporaryRow) + "," + direction + ")";
	    }

  
  public void drawGridLines(Graphics g) {
	    g.setColor(Color.LIGHT_GRAY);
		    drawHorizontalLines(g);
		    drawVerticalLines(g);
	}

	public void drawVerticalLines(Graphics g) {
		int line = 8;
	}
	    for (int see = 0; see <= line; see++) {
		 g.drawLine(20 + see * 20, 20, 20 + see * 20, 160);
	    }
	}

	public void drawHorizontalLines(Graphics g){
	    for (tmp = 0; tmp <= 7; tmp++) {
		  g.drawLine(20, 20 + tmp * 20, 180, 20 + tmp * 20);
	    }
	}

  public static int getInt() {
    BufferedReader r = new BufferedReader(new InputStreamReader(System.in));
    do {
      try {
        return Integer.parseInt(r.readLine());
      } catch (Exception e) {
      }
    } while (true);
  }
}
